import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/modules/about/views/about_view.dart';
import 'package:lapi_app/app/modules/home/controllers/home_controller.dart';
import 'package:lapi_app/app/routes/app_pages.dart';
import 'package:line_icons/line_icons.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      maintainBottomViewPadding: true,
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Color(0xFFFFFFFF),
          body: Column(
            children: [
              buildAppBar(),
              Expanded(
                  child: TabBarView(
                children: [buildProfileView(), buildImagePicker(), AboutView()],
              ))
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Color(0xFFF9A74D),
            onPressed: () {
              controller.pickImage();
            },
            child: Image.asset(
              "assets/icons/scanner.png",
              height: 25,
              fit: BoxFit.cover,
              // color: Color(0xFFF9A74D),
            ),
          ),
          // bottomNavigationBar: BottomAppBar(
          //   child: Container(
          //     padding: EdgeInsets.all(8.0),
          //     margin: EdgeInsets.all(8.0),
          //     height: 70.0,
          //     decoration: BoxDecoration(
          //       color: Color(0xFF7C7C7C),
          //       borderRadius: BorderRadius.circular(5.0),
          //       boxShadow: [
          //         BoxShadow(
          //           color: Color(0xFF30281F),
          //           offset: Offset(2, 2),
          //           blurRadius: 4,
          //         )
          //       ],
          //     ),
          //     child: Padding(
          //       padding: const EdgeInsets.all(8.0),
          //       child: Text(
          //         '( LAPI Systo V 1.0.0 )\nSystème de Lecture Automatique \nde Plaque d\'Immatriculation',
          //         textAlign: TextAlign.justify,
          //         style: TextStyle(
          //           fontSize: 12,
          //           color: Color(0xFFFCEFE1),
          //           fontWeight: FontWeight.w500,
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
        ),
      ),
    );
  }

  Column buildProfileView() {
    return Column(
      children: [
        SizedBox(height: 10.0),
        Text(
          'Bienvenu...',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            color: Color(0xFF3B3B3B),
            fontWeight: FontWeight.w600,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 8.0, bottom: 8.0),
          height: 80.0,
          width: 80.0,
          decoration: BoxDecoration(
              color: Color(0xFFECECEB),
              shape: BoxShape.circle,
              border: Border.all(width: 2.0, color: Color(0xFFF9A74D)),
              boxShadow: [
                BoxShadow(
                  color: Color(0xFFCECECE),
                  offset: Offset(2, 2),
                  blurRadius: 4,
                )
              ]),
          child: Icon(
            LineIcons.user,
            size: 35.0,
          ),
        ),
        Text(
          '${controller.user.value.user.email}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 14,
            color: Color(0xFF727272),
            fontWeight: FontWeight.w600,
          ),
        ),
        Text(
          '${controller.user.value.user.name}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 24,
            color: Color(0xFF252525),
            fontWeight: FontWeight.w600,
          ),
        ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: [
        //     Icon(LineIcons.location_arrow, size: 18.0),
        //     Text(
        //       'Rond-point INSTIGO',
        //       textAlign: TextAlign.center,
        //       style: TextStyle(
        //         fontSize: 14,
        //         color: Color(0xFF5A5A5A),
        //         fontWeight: FontWeight.w600,
        //       ),
        //     ),
        //   ],
        // ),
        SizedBox(height: 10.0),
        SizedBox(
          height: 40.0,
          width: 160,
          child: RaisedButton(
            color: Color(0xFF25211D),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            onPressed: () {
              Get.toNamed(Routes.RESULT);
            },
            child: Text(
              'Se déconnecter',
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFFF9A74D)),
            ),
          ),
        )
      ],
    );
  }

  Widget buildImagePicker() {
    return ListView(
      children: [
        Container(
          height: 150,
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Color(0xFFF0F0F0),
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: GetBuilder(
            init: HomeController.to,
            builder: (_) {
              if (HomeController.to.imageFile == null) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      LineIcons.image,
                      size: 70.0,
                      color: Color(0xFFCFCFCF),
                    ),
                    Text(
                      'Faites une capture \nou Importer une photo pour la vérification',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF838282),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                );
              } else {
                return Image.file(
                  HomeController.to.imageFile,
                  fit: BoxFit.cover,
                );
              }
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 60.0,
            // width: Get.context.width - 30,
            child: RaisedButton(
              color: Color(0xFF363636),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              onPressed: () {
                if (controller.imageFile == null) {
                  controller.pickImage();
                } else {
                  Get.toNamed(Routes.RESULT,
                      arguments: {"photo": controller.imageFile});
                }
              },
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(LineIcons.camera_retro,
                      size: 30.0, color: Color(0xFFF9A74D)),
                  SizedBox(width: 5.0),
                  GetBuilder(
                      init: HomeController.to,
                      builder: (_) {
                        return Text(
                          controller.imageFile == null
                              ? 'Capturer une plaque'
                              : 'Vérifier une plaque',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFFF8DEC2),
                          ),
                        );
                      }),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 60.0,
            // width: Get.context.width - 30,
            child: RaisedButton(
              color: Color(0xFF363636),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              onPressed: () {},
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(LineIcons.bug, size: 30.0, color: Color(0xFFF9A74D)),
                  SizedBox(width: 5.0),
                  Text(
                    'Signaler un cas',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFFF8DEC2),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 60.0,
            // width: Get.context.width - 30,
            child: RaisedButton(
              color: Color(0xFF363636),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2.0),
              ),
              onPressed: () {},
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(LineIcons.bell, size: 30.0, color: Color(0xFFF9A74D)),
                  SizedBox(width: 5.0),
                  Text(
                    'Notifications',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFFF8DEC2),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Container buildAppBar() {
    return Container(
      // padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        color: Color(0xFFFFFFFF),
        border: Border(
          bottom: BorderSide(
            color: Color(0xFFEBEBEB),
          ),
        ),
      ), //0xFF25211D
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 15.0),
                child: Text(
                  'LAPI Systo',
                  style: TextStyle(
                      fontSize: 19,
                      color: Color(0xFF25211D),
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 15.0),
                child: Image.asset(
                  "assets/icons/scanner.png",
                  height: 28,
                  // color: Color(0xFF25211D),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Système de Lecture Automatique \nde Plaque d\'Immatriculation',
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: 12,
                color: Color(0xFF25211D),
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          TabBar(
            indicatorColor: Color(0xFFF9A74D),
            unselectedLabelColor: Color(0xFFB1B0AF),
            labelColor: Color(0xFF25211D),
            indicatorWeight: 3.0,
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(color: Color(0xFFF9A74D), width: 3.0),
                insets: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0)),
            tabs: [
              Tab(
                icon: Icon(
                  LineIcons.user,
                ),
              ),
              Tab(
                icon: Icon(LineIcons.camera),
              ),
              Tab(
                icon: Icon(LineIcons.question),
              ),
            ],
          )
        ],
      ),
    );
  }
}
