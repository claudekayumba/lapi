import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lapi_app/app/models/code_case.dart';
import 'package:lapi_app/app/models/user.dart';
import 'package:lapi_app/app/services/http_service.dart';

enum AppState {
  free,
  picked,
  cropped,
}

class HomeController extends GetxController {
  final AppState state = AppState.free.obs();
  File imageFile;
  final picker = ImagePicker();
  final user = UserResult().obs;
  final isLoading = false.obs;
  List<Categories> codes;

  static HomeController get to => Get.find<HomeController>();

  @override
  // ignore: must_call_super
  void onInit() {
    user(Get.arguments['userInfo'] as UserResult);
    HttpService.userToken = user.value.token;
    getCategories();
  }

  

  @override
  // ignore: must_call_super
  void onReady() {}

  @override
  void onClose() {}

  Future<Null> pickImage({bool isCamera = true}) async {
    final pickedFile = await picker.getImage(source: isCamera ? ImageSource.camera : ImageSource.gallery);
    if (pickedFile != null) {
      imageFile = File(pickedFile.path);
      update();
      cropImage();
    } else {
      print('No image selected.');
    }
  }

  Future<Null> cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
      imageFile = croppedFile;
      state.obs.value = AppState.cropped;
    }
  }

  void clearImage() {
    imageFile = null;
    state.obs.value = AppState.free;
  }

  getCategories() async {
    try {
      isLoading(true);
      Response response = await HttpService.client
          .post(
        HttpService.CATEGORIES_SERVICE,
        options: Options(
            headers: {"X-Requested-With": "XMLHttpRequest"},
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
        // onSendProgress: (int sent, int total) {
        // percentReceive((sent * 100) / total);
        // },
      )
          .catchError(
        (onError) {
          isLoading(false);
        },
      );

      if (response.statusCode == 200) {
        print(response..data);
        codes = CategoriesResult.fromJson(response.data).categories;
      } else {
        // errorMessage("Information introuvable");
      }
    } finally {
      isLoading(false);
    }
  }
}
