import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/models/verification.dart';
import 'package:lapi_app/app/services/http_service.dart';

class ResultController extends GetxController {
  final errorMessage = "".obs;
  final percentReceive = 0.0.obs;
  final isLoading = false.obs;
  final isSucces = false.obs;
  final File imageFile = Get.arguments['photo'];
  final resultCheck = VerificationPlaqueResult().obs;

  @override
  // ignore: must_call_super
  Future<void> onInit() async {
    await verifierPlaque();
  }

  @override
  // ignore: must_call_super
  void onReady() {}

  @override
  void onClose() {}

  verifierPlaque() async {
    try {
      isLoading(true);
      isSucces(false);
      errorMessage("");
      percentReceive(0.0);

      String fileName = imageFile.path.split("/").last;

      FormData formData = new FormData.fromMap({
        "photo": await MultipartFile.fromFile(
          imageFile.path,
          filename: fileName,
        ),
      });
      Response response = await HttpService.client.post(
        HttpService.LAPI_VERIFICATION_SERVICE,
        options: Options(
          headers: {
            "X-Requested-With": "XMLHttpRequest",
            "Authorization": "Bearer ${HttpService.userToken}"
          },
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          },
        ),
        data: formData,
        onSendProgress: (int sent, int total) {
          percentReceive(
            double.tryParse(((sent * 100) / total).round().toString()),
          );
        },
      ).catchError((onError) {
        isLoading(false);
      });

      if (response.statusCode == 200) {
        print(response.data);
        resultCheck(VerificationPlaqueResult.fromJson(response.data));
        isSucces(true);
      } else {
        errorMessage("Information introuvable");
      }
    } finally {
      isLoading(false);
    }
  }
}
