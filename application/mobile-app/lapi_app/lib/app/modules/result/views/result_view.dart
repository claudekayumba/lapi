import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/modules/result/controllers/result_controller.dart';
import 'package:lapi_app/app/modules/result/views/owner_screen.dart';
import 'package:line_icons/line_icons.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ResultView extends GetView<ResultController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          body: Column(
            children: [
              Container(
                padding: EdgeInsets.all(8.0),
                margin: EdgeInsets.all(8.0),
                height: 165.0,
                width: Get.context.width,
                decoration: BoxDecoration(
                  color: Color(0xFF1F1A15),
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFFF9A74D),
                      offset: Offset(2, 2),
                      blurRadius: 4,
                    )
                  ],
                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          height: 40.0,
                          width: 40.0,
                          child: RawMaterialButton(
                            onPressed: () {
                              Get.back();
                            },
                            child: Icon(
                              LineIcons.arrow_left,
                              color: Color(0xFFF9A74D),
                            ),
                          ),
                        ),
                        Text(
                          'Resultat de la recherche',
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xFFF9A74D),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      'Numéro de plaque détecté',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 14,
                        color: Color(0xFFA1A1A1),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    GetX(
                      builder: (_) {
                        if (controller.isLoading.value) {
                          return Column(
                            children: [
                              Animator<double>(
                                tween: Tween<double>(begin: 35, end: 40),
                                cycles: 0,
                                // duration: Duration(seconds: 5),
                                builder: (context, animatorState, child) =>
                                    Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: CircularPercentIndicator(
                                      radius: 40.0,
                                      lineWidth: 2.5,
                                      percent:
                                          controller.percentReceive.value / 100,
                                      center: new Text(
                                        "${controller.percentReceive.value}%",
                                        style: TextStyle(
                                            color: Color(0xFFF9A74D),
                                            fontSize: 8.0),
                                      ),
                                      progressColor: Color(0xFFF9A74D),
                                    ),
                                  ),
                                ),
                              ),
                              // new Text(
                              //   'Vérification de plaque...',
                              //   textAlign: TextAlign.center,
                              //   style: TextStyle(
                              //     fontSize: 13,
                              //     color: Color(0xFFA38261),
                              //     fontWeight: FontWeight.w500,
                              //   ),
                              // )
                            ],
                          );
                        } else {
                          if (controller.isSucces.value) {
                            var car = controller.resultCheck.value
                                ?.immatriculations[0]?.vehicule;
                            return Column(
                              children: [
                                if (car != null)
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      controller.resultCheck.value
                                          ?.immatriculations[0]?.plaque?.numero
                                          ?.toUpperCase(),
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                          fontSize: 24,
                                          color: Color(0xFFF9A74D),
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 5),
                                    ),
                                  ),
                                if (car != null)
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      buildResultInfo(
                                          'Marque', '${car.marque}'),
                                      buildResultInfo('Modele', '${car.model}'),
                                      buildResultInfo(
                                          'Fabriquant', '${car.fabricant}'),
                                    ],
                                  )
                              ],
                            );
                          } else {
                            return Text(
                              'Numéro de plaque non détecté',
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFFA1A1A1),
                                fontWeight: FontWeight.w500,
                              ),
                            );
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
              TabBar(
                indicatorColor: Color(0xFFF9A74D),
                unselectedLabelColor: Color(0xFFB1B0AF),
                labelColor: Color(0xFF25211D),
                indicatorWeight: 3.0,
                indicator: UnderlineTabIndicator(
                    borderSide:
                        BorderSide(color: Color(0xFFF9A74D), width: 3.0),
                    insets: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0)),
                tabs: [
                  Tab(
                    child: Text('Propriétaire'),
                  ),
                  Tab(
                    child: Text('Autres info'),
                  ),
                ],
              ),
              GetX(builder: (snapshot) {
                if (!controller.isSucces.value) {
                  return Container();
                }
                return Expanded(
                  child: PageView(
                    children: [
                      buildPageWidget(
                        child: OwnerScreen(
                          owner: controller.resultCheck.value
                              .immatriculations[0].vehicule.owner,
                        ),
                      ),
                      buildPageWidget(
                        child: OwnerScreen(
                          owner: controller.resultCheck.value
                              .immatriculations[0].vehicule.owner,
                        ),
                      ),
                    ],
                  ),
                );
              })
            ],
          ),
        ),
      ),
    );
  }

  Widget buildResultInfo([String label, value]) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$label',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 14,
              color: Color(0xFFC5C5C5),
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            '$value',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 12,
              color: Color(0xFFFFFFFF),
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPageWidget({Widget child}) {
    return Container(
        padding: EdgeInsets.all(8.0),
        margin: EdgeInsets.all(15.0),
        // height: 70.0,
        width: Get.context.width,
        decoration: BoxDecoration(
          color: Color(0xFFDFDFDF),
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFBDBDBD),
              offset: Offset(2, 2),
              blurRadius: 4,
            )
          ],
        ),
        child: child);
  }
}
