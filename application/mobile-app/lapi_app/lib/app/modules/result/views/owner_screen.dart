import 'package:flutter/material.dart';
import 'package:lapi_app/app/models/owner.dart';
class OwnerScreen extends StatelessWidget {
  final Owner owner;

  const OwnerScreen({Key key, this.owner}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildResultInfo('Identité', '${owner.completName}'),
            buildResultInfo('Sexe', '${owner.gender}'),
            buildResultInfo('Date de naissance', '${owner.birthday}'),
            buildResultInfo('Lieu de naissance', '${owner.birthLocation}'),
            buildResultInfo('Adresse', '${owner.adresse}'),
            buildResultInfo('Contact', '${owner.adresse}'),
          ],
        ),
      
    );
  }

  Widget buildResultInfo([String label, value]) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$label',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 14,
              color: Color(0xFF252525),
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            '$value',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 12,
              color: Color(0xFF252525),
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
