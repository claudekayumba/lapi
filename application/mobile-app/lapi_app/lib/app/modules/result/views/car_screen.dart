import 'package:flutter/material.dart';
import 'package:lapi_app/app/models/vehicule.dart';

class CarScreen extends StatelessWidget {
  final Vehicule vehicule;

  const CarScreen({Key key, this.vehicule}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildResultInfo('Marque', '${vehicule.marque}'),
            buildResultInfo('Modele', '${vehicule.model}'),
            buildResultInfo('Fabriquant', '${vehicule.fabricant}'),
            buildResultInfo('Date de Fabrication', '${vehicule.dateFabrication}'),
          ],
        ),
    );
  }

  Widget buildResultInfo([String label, value]) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$label',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 14,
              color: Color(0xFF252525),
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            '$value',
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: 12,
              color: Color(0xFF252525),
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
