import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/modules/login/controllers/login_controller.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF1D1916),
        body: Container(
          decoration: BoxDecoration(color: Color(0xFF1D1916)), //0xFF25211D
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/icons/scanner.png",
                    height: 50,
                    // color: Color(0xFFF9A74D),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'LAPI Systo',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF807B77),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    height: 400,
                    padding: const EdgeInsets.all(20.0),
                    margin: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Color(0xFF25211D),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Authentification',
                            style: TextStyle(
                              fontSize: 19,
                              color: Color(0xFFF9A74D),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        // buildTextField(title: 'Point de contrôle', controller: controller.userTxt),
                        buildTextField(
                          title: 'Identifiant',
                          controller: controller.userTxt,
                        ),
                        buildTextField(
                          title: 'Mot de passe',
                          isPasword: true,
                          controller: controller.passwordTxt,
                        ),

                        GetX(
                          builder: (_) {
                            if (controller.errorMessage.value != "") {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  '${controller.errorMessage.value}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Color(0xFFA38261),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              );
                            }
                            return SizedBox(height: 8.0);
                          },
                        ),
                        GetX(builder: (_) {
                          if (controller.isLoading.value) {
                            return Column(
                              children: [
                                new CircularPercentIndicator(
                                  radius: 50.0,
                                  lineWidth: 3.0,
                                  percent:
                                      controller.percentReceive.value / 100,
                                  center: new Text(
                                    "${controller.percentReceive.value}%",
                                    style: TextStyle(
                                        color: Color(0xFFF9A74D),
                                        fontSize: 8.0),
                                  ),
                                  progressColor: Color(0xFFF9A74D),
                                ),
                                new Text(
                                  'Connexion...',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Color(0xFFA38261),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            );
                          }
                          return SizedBox(
                            height: 45.0,
                            width: Get.context.width - 30,
                            child: RaisedButton(
                              color: Color(0xFFF9A74D),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)),
                              onPressed: () {
                                controller.login();
                              },
                              child: Text(
                                'Se connecter',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          );
                        }),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Système de Lecture Automatique \nde Plaque d\'Immatriculation\nCopy Right 2020',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 13,
                        color: Color(0xFFA38261),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container buildTextField(
      {String title,
      TextEditingController controller,
      bool isPasword = false}) {
    return Container(
      height: 50.0,
      width: Get.context.width - 30,
      margin: const EdgeInsets.all(2.0),
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Color(0xFF3D3833),
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: TextField(
        controller: controller,
        textAlign: TextAlign.center,
        obscureText: isPasword,
        decoration: InputDecoration(
            hintText: '$title',
            hintStyle: TextStyle(
              fontSize: 16,
              color: Color(0xFF757575),
              fontWeight: FontWeight.w600,
            ),
            border: InputBorder.none),
        style: TextStyle(
          fontSize: 16,
          color: Color(0xFFB3B3B3),
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
