import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/models/user.dart';
import 'package:lapi_app/app/routes/app_pages.dart';
import 'package:lapi_app/app/services/http_service.dart';

class LoginController extends GetxController {
  final count = 0.obs;
  final errorMessage = "".obs;
  final percentReceive = 0.0.obs;
  final isLoading = false.obs;

  final TextEditingController userTxt = TextEditingController();
  final TextEditingController passwordTxt = TextEditingController();

  @override
  // ignore: must_call_super
  void onInit() {}

  @override
  // ignore: must_call_super
  void onReady() {}

  @override
  void onClose() {}

  login() async {
    try {
      isLoading(true);
      errorMessage("");

      FormData formData = new FormData.fromMap({
        "device_name": "XP-460",
        "email": userTxt.text,
        "password": passwordTxt.text,
      });
      Response response = await HttpService.client.post(
        HttpService.LOGIN_SERVICE,
        options: Options(
            headers: {"X-Requested-With": "XMLHttpRequest"},
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
        data: formData,
        onSendProgress: (int sent, int total) {
          percentReceive((sent * 100) / total);
        },
      ).catchError(
        (onError) {
          isLoading(false);
          errorMessage("Problème de connexion");
        },
      );

      if (isLoading.value) {
        if (response.statusCode == 200) {
          Get.offAndToNamed(Routes.HOME,
              arguments: {"userInfo": UserResult.fromJson(response.data)});
        } else {
          errorMessage("User ID or password incorrect");
        }
      }
    } finally {
      isLoading(false);
      percentReceive(0);
    }
  }
}
