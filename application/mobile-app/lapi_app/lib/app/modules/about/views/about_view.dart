import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/modules/about/controllers/about_controller.dart';

class AboutView extends GetView<AboutController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/icons/scanner.png",
              height: 70,
              // color: Color(0xFFF9A74D),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'LAPI Systo',
                style: TextStyle(
                    letterSpacing: 3,
                    fontSize: 24,
                    color: Color(0xFF25211D),
                    fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              "LAPI Systo est un système de Lecture Automatique des plaques d'immatriculation des véhicules grâce à la photo de plaque prise à partir d'une application mobile",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}
