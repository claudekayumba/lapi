import 'dart:async';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:lapi_app/app/routes/app_pages.dart';

class SplashController extends GetxController {
  final count = 0.obs;

  static SplashController get to => Get.find();

  @override
  // ignore: must_call_super
  void onInit() {
    init();
  }

  @override
  // ignore: must_call_super
  void onReady() {}

  @override
  void onClose() {}

  void init() {
    Timer(
      new Duration(seconds: 3),
      () async {
        Get.offAndToNamed(Routes.LOGIN);
        SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      },
    );
  }

  void increment() => count.value++;
}
