import 'package:flutter/material.dart';
import 'package:get/get.dart'; 
import 'package:lapi_app/app/modules/splash/controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  final ctrl = Get.find<SplashController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: Color(0xFFF9A74D)), //0xFF25211D
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'LAPI Systo',
                  style: TextStyle(
                      fontSize: 24,
                      color: Color(0xFF25211D),
                      fontWeight: FontWeight.bold),
                ),
              ),
              Image.asset(
                "assets/icons/scanner.png",
                height: 150,
                // color: Color(0xFFF9A74D),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Système de Lecture Automatique \nde Plaque d\'Immatriculation',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16,
                      color: Color(0xFF25211D),
                      fontWeight: FontWeight.w500,),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
  