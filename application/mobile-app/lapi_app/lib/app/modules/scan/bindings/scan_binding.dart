import 'package:get/get.dart';

import 'package:lapi_app/app/modules/scan/controllers/scan_controller.dart';

class ScanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ScanController>(
      () => ScanController(),
    );
  }
}
