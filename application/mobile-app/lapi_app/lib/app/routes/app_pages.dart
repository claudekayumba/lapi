import 'package:lapi_app/app/modules/result/views/result_view.dart';
import 'package:lapi_app/app/modules/result/bindings/result_binding.dart';
import 'package:lapi_app/app/modules/scan/views/scan_view.dart';
import 'package:lapi_app/app/modules/scan/bindings/scan_binding.dart';
import 'package:lapi_app/app/modules/about/views/about_view.dart';
import 'package:lapi_app/app/modules/about/bindings/about_binding.dart';
import 'package:lapi_app/app/modules/login/views/login_view.dart';
import 'package:lapi_app/app/modules/login/bindings/login_binding.dart';
import 'package:lapi_app/app/modules/splash/views/splash_view.dart';
import 'package:lapi_app/app/modules/splash/bindings/splash_binding.dart';
import 'package:lapi_app/app/modules/home/views/home_view.dart';
import 'package:lapi_app/app/modules/home/bindings/home_binding.dart';
import 'package:get/get.dart';
part 'app_routes.dart';

class AppPages {
  
static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: Routes.HOME, 
      page:()=> HomeView(), 
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.SPLASH, 
      page:()=> SplashView(), 
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.LOGIN, 
      page:()=> LoginView(), 
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.ABOUT, 
      page:()=> AboutView(), 
      binding: AboutBinding(),
    ),
    GetPage(
      name: Routes.SCAN, 
      page:()=> ScanView(), 
      binding: ScanBinding(),
    ),
    GetPage(
      name: Routes.RESULT, 
      page:()=> ResultView(), 
      binding: ResultBinding(),
    ),
  ];
}