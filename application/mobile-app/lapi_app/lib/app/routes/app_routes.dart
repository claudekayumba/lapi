part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes{

  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const ABOUT = '/about';
  static const SCAN = '/scan';
  static const RESULT = '/result';
}