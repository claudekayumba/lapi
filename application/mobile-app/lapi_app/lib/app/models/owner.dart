class Owner {
  int id;
  String completName;
  String gender;
  String birthday;
  String birthLocation;
  String adresse;
  String telephone;
  String createdAt;
  String updatedAt;
  Null deletedAt;

  Owner(
      {this.id,
      this.completName,
      this.gender,
      this.birthday,
      this.birthLocation,
      this.adresse,
      this.telephone,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  Owner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    completName = json['complet_name'];
    gender = json['gender'];
    birthday = json['birthday'];
    birthLocation = json['birth_location'];
    adresse = json['adresse'];
    telephone = json['telephone'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['complet_name'] = this.completName;
    data['gender'] = this.gender;
    data['birthday'] = this.birthday;
    data['birth_location'] = this.birthLocation;
    data['adresse'] = this.adresse;
    data['telephone'] = this.telephone;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}