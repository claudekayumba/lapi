class CategoriesResult {
  List<Categories> categories;

  CategoriesResult({this.categories});

  CategoriesResult.fromJson(Map<String, dynamic> json) {
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categories {
  int id;
  String code;
  String color;
  String description;
  String createdAt;
  String updatedAt;
  Null deletedAt;

  Categories(
      {this.id,
      this.code,
      this.color,
      this.description,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    color = json['color'];
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['color'] = this.color;
    data['description'] = this.description;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}