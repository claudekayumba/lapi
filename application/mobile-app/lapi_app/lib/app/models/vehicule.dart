
import 'package:lapi_app/app/models/owner.dart';

class Vehicule {
  int id;
  var ownerId;
  String fabricant;
  String marque;
  String model;
  String dateFabrication;
  String typeVehicule;
  String createdAt;
  String updatedAt;
  var deletedAt;
  Owner owner;

  Vehicule(
      {this.id,
      this.ownerId,
      this.fabricant,
      this.marque,
      this.model,
      this.dateFabrication,
      this.typeVehicule,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.owner});

  Vehicule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ownerId = json['owner_id'];
    fabricant = json['fabricant'];
    marque = json['marque'];
    model = json['model'];
    dateFabrication = json['date_fabrication'];
    typeVehicule = json['type_vehicule'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['owner_id'] = this.ownerId;
    data['fabricant'] = this.fabricant;
    data['marque'] = this.marque;
    data['model'] = this.model;
    data['date_fabrication'] = this.dateFabrication;
    data['type_vehicule'] = this.typeVehicule;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.owner != null) {
      data['owner'] = this.owner.toJson();
    }
    return data;
  }
}