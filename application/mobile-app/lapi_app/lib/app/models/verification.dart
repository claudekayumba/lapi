import 'package:lapi_app/app/models/vehicule.dart';

class VerificationPlaqueResult {
  List<Immatriculations> immatriculations;

  VerificationPlaqueResult({this.immatriculations});

  VerificationPlaqueResult.fromJson(Map<String, dynamic> json) {
    if (json['immatriculations'] != null) {
      immatriculations = new List<Immatriculations>();
      json['immatriculations'].forEach((v) {
        immatriculations.add(new Immatriculations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.immatriculations != null) {
      data['immatriculations'] =
          this.immatriculations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Immatriculations {
  int id;
  var plaqueId;
  var vehiculeId;
  String numImmatriculation;
  String dateImmatriculation;
  String motifImmatriculation;
  String lieuImmatriculation;
  String createdAt;
  String updatedAt;
  var deletedAt;
  Plaque plaque;
  Vehicule vehicule;

  Immatriculations(
      {this.id,
      this.plaqueId,
      this.vehiculeId,
      this.numImmatriculation,
      this.dateImmatriculation,
      this.motifImmatriculation,
      this.lieuImmatriculation,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.plaque,
      this.vehicule});

  Immatriculations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    plaqueId = json['plaque_id'];
    vehiculeId = json['vehicule_id'];
    numImmatriculation = json['num_immatriculation'];
    dateImmatriculation = json['date_immatriculation'];
    motifImmatriculation = json['motif_immatriculation'];
    lieuImmatriculation = json['lieu_immatriculation'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    plaque =
        json['plaque'] != null ? new Plaque.fromJson(json['plaque']) : null;
    vehicule = json['vehicule'] != null
        ? new Vehicule.fromJson(json['vehicule'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['plaque_id'] = this.plaqueId;
    data['vehicule_id'] = this.vehiculeId;
    data['num_immatriculation'] = this.numImmatriculation;
    data['date_immatriculation'] = this.dateImmatriculation;
    data['motif_immatriculation'] = this.motifImmatriculation;
    data['lieu_immatriculation'] = this.lieuImmatriculation;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.plaque != null) {
      data['plaque'] = this.plaque.toJson();
    }
    if (this.vehicule != null) {
      data['vehicule'] = this.vehicule.toJson();
    }
    return data;
  }
}

class Plaque {
  int id;
  var typeId;
  String numero;
  String modelPlaque;
  String dateLivraison;
  String lieuAffectation;
  String createdAt;
  String updatedAt;
  Null deletedAt;

  Plaque(
      {this.id,
      this.typeId,
      this.numero,
      this.modelPlaque,
      this.dateLivraison,
      this.lieuAffectation,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  Plaque.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    typeId = json['type_id'];
    numero = json['numero'];
    modelPlaque = json['model_plaque'];
    dateLivraison = json['date_livraison'];
    lieuAffectation = json['lieu_affectation'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type_id'] = this.typeId;
    data['numero'] = this.numero;
    data['model_plaque'] = this.modelPlaque;
    data['date_livraison'] = this.dateLivraison;
    data['lieu_affectation'] = this.lieuAffectation;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}
