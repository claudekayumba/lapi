import 'package:dio/dio.dart';

class HttpService {
  static BaseOptions _options = new BaseOptions(
    baseUrl: "https://lapisysteme.t-tbs.com/api/mobile",
    // connectTimeout: 5000,
    // receiveTimeout: 3000,
  );

  static String userToken = "";

  static const String LOGIN_SERVICE = "/login";

  static const String LOGOUT_SERVICE = "/logout";

  static const String LAPI_VERIFICATION_SERVICE = "/plaque/verifications";

  static const String SEND_CASE_SERVICE = "/send-case";

  static const String CATEGORIES_SERVICE = "/cases/categoties";

  static final Dio _dio = new Dio(_options);

  static Dio get client => _dio;
}
